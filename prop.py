team_map={
            'Sunrisers Hyderabad': ['#DAF7A6','SRH'],
            'Rising Pune Supergiant': ['#E6B043','RPS'],
            'Kolkata Knight Riders': ['#FF5733', 'KKR'],
            'Kings XI Punjab': ['#AF64A9', 'PUNJAB'],
            'Royal Challengers Bangalore': ['#307B7E', 'RCB'],
            'Mumbai Indians': ['#06024C', 'MI'],
            'Chennai Super Kings': ['#F9F504', 'CSK'],
            'Delhi Daredevils': ['#EC071C', 'DD'],
            'Rajasthan Royals': ['#07ECDB', 'RR'],
            'Gujarat Lions': ['#ADADBC', 'GL' ],
            'Deccan Chargers': ['#F96804', 'DC'],
            'Pune Warriors': ['#030834', 'PW'],
            'Kochi Tuskers Kerala': ['#83E882', 'KTK'],
            'Rising Pune Supergiants': ['#040CF9', "RPS's"]
        }