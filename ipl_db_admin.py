import psycopg2


def no_of_matches_per_year_sql():
    con = psycopg2.connect(database="test_ipl", user="gursharan")
    cur = con.cursor()
    stmt = '''select season, count(*)
    from mock_matches_data group by season order by season;'''
    cur.execute(stmt)
    no_matches_year = dict(cur.fetchall())
    return no_matches_year


def matches_won_by_teams_sql():
    con = psycopg2.connect(database="test_ipl", user="gursharan")
    cur = con.cursor()
    stmt = '''select winner, season, count(*)
            from mock_matches_data
            group by winner, season
            order by winner
            ;'''
    cur.execute(stmt)
    team_no_of_wins = cur.fetchall()
    team_no_of_wins = convert_tup_to_dictionary(team_no_of_wins)
    print(team_no_of_wins)
    return team_no_of_wins


def extra_runs_data_sql(year):
    con = psycopg2.connect(database="test_ipl", user="gursharan")
    cur = con.cursor()
    stmt = '''select bowling_team, SUM(total_runs)
             from mock_deliveries_data
             where match_id in
             (select id from mock_matches_data where season = %s)
             group by bowling_team;
             '''
    cur.execute(stmt, [year])
    runs_conceeded = cur.fetchall()
    print(runs_conceeded)
    runs_conceeded = dict(runs_conceeded)
    return runs_conceeded


def get_economic_bowlers_sql(year):
    con = psycopg2.connect(database="test_ipl", user="gursharan")
    cur = con.cursor()
    stmt = '''select bowler, sum(total_runs),
    count(CASE WHEN wide_runs = 1 or noball_runs=1 THEN NULL ELSE 0 END)
            from mock_deliveries_data
                where match_id in
                (select id from mock_matches_data where season = %s)
                group by bowler
             '''
    cur.execute(stmt, [year])
    bowler_data = cur.fetchall()
    print(bowler_data)
    bowlers_economy = convert_to_bowlers_economy(bowler_data)
    return bowlers_economy


def get_top_strike_rates_sql(from_year, to_year):
    con = psycopg2.connect(database="test_ipl", user="gursharan")
    cur = con.cursor()
    stmt = '''select batsman, sum(total_runs)-sum(extra_runs),
    count(CASE WHEN wide_runs = 1 or noball_runs=1 THEN NULL ELSE 0 END)
            from mock_deliveries_data
                where match_id in
                (select id from mock_matches_data
                where season between %s and %s)
                group by batsman
             '''
    cur.execute(stmt, [from_year, to_year])
    batsman_data = cur.fetchall()
    print(batsman_data)
    batsman_strike_rate = convert_to_batsman_strike_rate(batsman_data)
    return batsman_strike_rate


def convert_tup_to_dictionary(team_no_of_wins):
    team_no_of_wins_dict = {}
    for key, s, w in team_no_of_wins:
        if key not in team_no_of_wins_dict:
            team_no_of_wins_dict[key] = {}
        team_no_of_wins_dict[key][str(s)] = w
    return team_no_of_wins_dict


def convert_to_bowlers_economy(bowler_data):
    bowlers_economy = {}
    for key, total_runs, balls in bowler_data:
        bowlers_economy[key] = total_runs/(balls/6)
    return bowlers_economy


def convert_to_batsman_strike_rate(batsman_data):
    batsman_strike_rate = {}
    for name, runs, balls in batsman_data:
        batsman_strike_rate[name] = (runs/balls)*100
    return batsman_strike_rate
