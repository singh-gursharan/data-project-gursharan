This project does the analysis of the ipl dataset from the year 2008 to the year 2019.
It shows the analysed data in the form of graph.

The project displays the five analysis:
1. The number of matches played per year of all the years in IPL.
2. Stacked bar chart of matches won of all teams over all the years of IPL.
3. For the year 2016 plot the extra runs conceded per team.
4. For the year 2015 plot the top economical bowlers.
5. Calculating the best player according to strike rate with in a perticular year of span.

Project structure:
All these functionality are initiated from a module name main.py
The project uses the external library matplotlib to display(plot) the analysed data in bar graph format
The project uses the helper module name helper.py for some common functionality that can be used by different queries.