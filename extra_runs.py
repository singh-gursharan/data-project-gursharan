from csv import DictReader
import matplotlib.pyplot as plt
import prop
import ipl_db_admin as admin


def extra_runs_data_main(matches_path, deliveries_path, season):
    runs_conceeded = extra_runs_data(matches_path, deliveries_path, season)
    extra_runs_data_grph(runs_conceeded)


def extra_runs_data_sql_main(year):
    runs_conceeded = admin.extra_runs_data_sql(year)
    extra_runs_data_grph(runs_conceeded)


def final_data(deliveries_path, match_id):
    runs_conceded = {}
    with open(deliveries_path, 'r') as f:
        csv_reader = DictReader(f)
        print(match_id)
        for row in csv_reader:
            row_match_id = row['match_id']
            row_total_runs = int(row['total_runs'])
            if row_match_id in match_id:
                print(row_match_id)
                bowling_team = row['bowling_team']
                if bowling_team in runs_conceded:
                    runs_conceded[bowling_team] += row_total_runs
                else:
                    runs_conceded[bowling_team] = row_total_runs
    print(runs_conceded)
    return runs_conceded


def extra_runs_data_grph(runs_conceded):
    team_map = prop.team_map
    teams = list(runs_conceded.keys())
    for i in range(0, len(teams)):
        teams[i] = team_map[teams[i]][1]
    total_runs = list(runs_conceded.values())
    plt.bar(teams, total_runs, 0.3)
    title = 'RUNS CONCEEDED'
    plt.xlabel('Teams')
    plt.ylabel('Total Runs')
    plt.title(title)
    plt.savefig("./graphs/" + title, quality=95, pad_inches=0, dpi=600)
    plt.show()


def extra_runs_data(matches_path, deliveries_path, season):
    match_id = []
    with open(matches_path, 'r') as f:
        csv_reader = DictReader(f)
        for row in csv_reader:
            year = int(row['season'])
            if year == season:
                match_id += [row['id']]
    print(match_id)
    runs_conceeded = final_data(deliveries_path, match_id)
    return runs_conceeded


extra_runs_data_sql_main(2016)
