import helper
import matplotlib.pyplot as plt
from csv import DictReader
import ipl_db_admin as admin


def economic_bowlers_main(matches_path, deliveries_path, year):
    bowlers_economy = get_economic_bowlers(matches_path, deliveries_path, year)
    economic_bowlers_graph(bowlers_economy, 6)


def economic_bowlers_sql_main(year):
    bowlers_economy = admin.get_economic_bowlers_sql(year)
    economic_bowlers_graph(bowlers_economy, 3)


def get_economic_bowlers(matches_path, deliveries_path, year):
    match_id = fetch_match_id(matches_path, year)
    bowlers_data = {}
    with open(deliveries_path, 'r') as f:
        csv_reader = DictReader(f)
        for row in csv_reader:
            if row['match_id'] in match_id:
                bowler = row['bowler']
                row_total_runs = int(row['total_runs'])
                wide_ball = int(row['wide_runs'])
                no_ball = int(row['noball_runs'])

                if bowler in bowlers_data:
                    bowlers_data[bowler]['runs'] += row_total_runs
                    if(wide_ball == 0 and no_ball == 0):
                        bowlers_data[bowler]['deliveries'] += 1
                else:
                    bowlers_data[bowler] = {}
                    bowlers_data[bowler]['runs'] = row_total_runs
                    if(wide_ball == 0 and no_ball == 0):
                        bowlers_data[bowler]['deliveries'] = 1
                    else:
                        bowlers_data[bowler]['deliveries'] = 0

    print(bowlers_data)
    bowlers_economy = fetch_bowler_economy(bowlers_data)
    # Sorting the economy in ascending order to get the top economical bowlers
    tups = sorted(bowlers_economy.items(), key=lambda x: x[1])
    bowlers_economy = {}
    print(bowlers_economy)
    helper.convert(tups, bowlers_economy)
    print(bowlers_economy)
    return bowlers_economy


def fetch_match_id(matches_path, season):
    match_id = []
    with open(matches_path, 'r') as f:
        csv_reader = DictReader(f)
        for row in csv_reader:
            year = int(row['season'])
            if year == season:
                match_id += [row['id']]
    print(match_id)
    return match_id


def fetch_bowler_economy(bowlers_data):
    bowlers_economy = {}
    for name in bowlers_data:
        bowlers_economy[name] = (bowlers_data[name]['runs'] /
                                 (bowlers_data[name]['deliveries']/6))
    return bowlers_economy


def economic_bowlers_graph(bowlers_economy, no_of_bowlers):
    bowler_name = [*bowlers_economy][:no_of_bowlers]
    economy = [*bowlers_economy.values()][:no_of_bowlers]
    title = "TOP ECONOMICAL BOWLERS"
    plt.bar(bowler_name, economy, 0.3)
    plt.xlabel('Name Of Bowler')
    plt.ylabel('Economy')
    plt.title(title)
    plt.savefig("./graphs/" + title, quality=95, pad_inches=0, dpi=600)
    plt.show()


economic_bowlers_sql_main(2016)
