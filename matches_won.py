from csv import DictReader
import matplotlib.pyplot as plt
import prop
import ipl_db_admin


def matches_won_by_teams_main(matches_path):
    team_no_of_wins = matches_won_by_teams(matches_path)
    print(team_no_of_wins)
    matches_won_by_teams_stack_graph(team_no_of_wins)


def matches_won_by_teams_sql_main():
    team_no_of_wins = ipl_db_admin.matches_won_by_teams_sql()
    matches_won_by_teams_stack_graph(team_no_of_wins)


def matches_won_by_teams(matches_path):
    winner_data = {}
    with open(matches_path, 'r') as f:
        csv_reader = DictReader(f)
        for row in csv_reader:
            season = row['season']
            team = row['winner']
            if(team == ''):
                team = 'tie'
            if(team in winner_data):
                if(season in winner_data[team]):
                    winner_data[team][season] += 1
                else:
                    winner_data[team][season] = 1
            else:
                winner_data[team] = {}
                winner_data[team][season] = 1
    print(winner_data)
    return winner_data


def matches_won_by_teams_stack_graph(team_no_of_wins):
    team_map = prop.team_map
    x = list(range(2008, 2018))
    team_wins_over_years = []
    last_team = [0]*(len(x))
    for team in team_no_of_wins:
        if(team == 'tie'):
            continue
        for year in range(2008, 2018):
            year = str(year)
            if year in team_no_of_wins[team]:
                team_wins_over_years += [team_no_of_wins[team][year]]
            else:
                team_wins_over_years += [0]
        plt.bar(x, team_wins_over_years, bottom=last_team, linewidth=1,
                color=team_map[team][0], label=team_map[team][1])
        print(team_wins_over_years)
        add(last_team, team_wins_over_years)
        team_wins_over_years = []
    title = 'NO OF MATCHES PER YEAR BY EACH TEAM'
    plt.xlabel('Year')
    plt.ylabel('Number Of Matches Won')
    plt.title(title)
    plt.legend(loc=1, ncol=2)
    plt.yticks(range(0, 110, 5))
    plt.savefig("./graphs/" + title, quality=95, pad_inches=0, dpi=600)
    plt.show()


def add(y, last_team):
    if len(last_team) == 0:
        return
    for i in range(0, len(y)):
        y[i] = y[i] + last_team[i]


matches_won_by_teams_sql_main()
