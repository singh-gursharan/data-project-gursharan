import economic_bowlers
import unittest


class Test_economical_bowlers(unittest.TestCase):

    def test_economical_bowlers(self):
        result = economic_bowlers.get_economic_bowlers(
            "./assets/matches_test.csv", "./assets/deliveries_test.csv", 2016)
        output = {
            'P Kumar': 3.0,
            'RP Singh': 8.0,
            'AD Russell': 12.0
            }
        self.assertEqual(result, output)


if __name__ == '__main__':
    unittest.main()
