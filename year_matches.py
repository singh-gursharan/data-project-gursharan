from csv import DictReader
import matplotlib.pyplot as plt
import ipl_db_admin


def no_of_matches_per_year_main(matches_path):
    no_matches_year = no_of_matches_per_year(matches_path)
    no_of_matches_graph(no_matches_year)


def no_of_matches_per_year_sql_main():
    no_matches_year = ipl_db_admin.no_of_matches_per_year_sql()
    no_of_matches_graph(no_matches_year)


def no_of_matches_per_year(matches_path):
    no_matches_year = {}
    with open(matches_path, 'r') as f:
        csv_reader = DictReader(f)
        for row in csv_reader:
            if(row['season'] in no_matches_year):
                no_matches_year[row['season']] += 1
            else:
                no_matches_year[row['season']] = 1
    return no_matches_year


def no_of_matches_graph(no_matches_year):
    x = list(no_matches_year.keys())
    print(x)
    y = list(no_matches_year.values())
    print(y)
    title = "NO OF IPL MATCHES WITH YEAR"
    plt.bar(x, y)
    plt.xlabel('year')
    plt.ylabel('no of matches')
    plt.title(title)
    plt.savefig("./graphs/" + title, quality=95, pad_inches=0, dpi=600)
    plt.show()


no_of_matches_per_year_sql_main()
