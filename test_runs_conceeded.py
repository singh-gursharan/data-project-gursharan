import extra_runs
import unittest


class Test_economical_bowlers(unittest.TestCase):
    def test_extra_runs(self):
        result = extra_runs.extra_runs_data(
            "./assets/matches_test.csv", "./assets/deliveries_test.csv", 2016)
        output = {
            'Rising Pune Supergiants': 8,
            'Kolkata Knight Riders': 6,
            'Gujarat Lions': 3
            }
        self.assertEqual(result, output)


if __name__ == '__main__':
    unittest.main()
