import matches_won
import unittest


class Test_matches_won(unittest.TestCase):
    def test_matches_won(self):
        result = matches_won.matches_won_by_teams("./assets/matches_test.csv")
        output = {
            'Sunrisers Hyderabad': {
                '2017': 2
            },
            'Rising Pune Supergiant': {
                '2017': 1
            },
            'Kolkata Knight Riders': {
                '2017': 1,
                '2008': 2,
                '2016': 1
            },
            'Kings XI Punjab': {
                '2017': 1,
            },
            'Royal Challengers Bangalore': {
                '2017': 1,
                '2008': 1
            },
            'Chennai Super Kings': {
                '2008': 1
            },
            'Delhi Daredevils': {
                '2008': 1
            },
            'Rajasthan Royals': {
                '2008': 1
            },
            'Rising Pune Supergiants': {
                '2016': 1
            },
            'Gujarat Lions': {
                '2016': 1
            }
        }
        self.assertEqual(result, output)


if __name__ == '__main__':
    unittest.main()
