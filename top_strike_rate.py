from csv import DictReader
import helper
import matplotlib.pyplot as plt
import ipl_db_admin as admin


def top_strike_rate_main(matches_path, deliveries_path, from_year, to_year):
    batsman_strike_rate = get_top_strike_rates(
        matches_path, deliveries_path, from_year, to_year)
    strike_rate_graph(batsman_strike_rate, 6, from_year, to_year)


def top_strike_rate_sql_main(from_year, to_year):
    batsman_strike_rate = admin.get_top_strike_rates_sql(from_year, to_year)
    strike_rate_graph(batsman_strike_rate, 6, from_year, to_year)


def fetch_match_id(matches_path, from_year, to_year):
    match_id = []
    with open(matches_path, 'r') as f:
        csv_reader = DictReader(f)
        for row in csv_reader:
            year = int(row['season'])
            if year >= from_year and year <= to_year:
                match_id += [row['id']]
    print(match_id)
    return match_id


def get_top_strike_rates(matches_path, deliveries_path, from_year, to_year):
    match_ids = fetch_match_id(matches_path, from_year, to_year)
    bats_man_data = {}
    with open(deliveries_path, 'r') as f:
        csv_reader = DictReader(f)
        for row in csv_reader:
            if row['match_id'] in match_ids:
                batsman = row['batsman']
                row_total_runs = int(row['total_runs'])
                wide_ball = int(row['wide_runs'])
                no_ball = int(row['noball_runs'])

                if batsman in bats_man_data:
                    if(wide_ball == 0 and no_ball == 0):
                        bats_man_data[batsman]['runs'] += row_total_runs
                        bats_man_data[batsman]['bowls_played'] += 1

                else:
                    bats_man_data[batsman] = {}
                    if(wide_ball == 0 and no_ball == 0):
                        bats_man_data[batsman]['runs'] = row_total_runs
                        bats_man_data[batsman]['bowls_played'] = 1
                    else:
                        bats_man_data[batsman]['runs'] = 0
                        bats_man_data[batsman]['bowls_played'] = 0
        print(bats_man_data)
        bats_man_data = fetch_strike_rate(bats_man_data)
        # Sorting the economy in ascending order to get the top economical
        # bowlers
        tups = sorted(bats_man_data.items(), key=lambda x: x[1], reverse=True)
        bats_man_data = {}
        print(bats_man_data)
        helper.convert(tups, bats_man_data)
        print(bats_man_data)
        return bats_man_data


def fetch_strike_rate(bats_man_data):
    batsman_strike_rate = {}
    for name in bats_man_data:
        batsman_strike_rate[name] = (bats_man_data[name]['runs'] /
                                     bats_man_data[name]['bowls_played'])*100
    return batsman_strike_rate


def strike_rate_graph(batsman_strike_rate, no_of_batsman, from_year, to_year):
    bowler_name = [*batsman_strike_rate][:no_of_batsman]
    economy = [*batsman_strike_rate.values()][:no_of_batsman]
    title = "TOP STRIKE RATES"
    plt.bar(bowler_name, economy, 0.3)
    plt.xlabel('Name Of Batsman')
    plt.ylabel('Strike Rate')
    plt.title('TOP STRIKE RATES OF TOP ' + str(no_of_batsman) + ' FROM ' +
              str(from_year) + ' to ' + str(to_year))
    plt.savefig("./graphs/" + title, quality=95, pad_inches=0, dpi=600)
    plt.rcParams['figure.figsize'] = 100, 800
    plt.show()


top_strike_rate_sql_main(2016, 2016)
