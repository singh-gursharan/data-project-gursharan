import year_matches
import matches_won
import extra_runs
import economic_bowlers
import top_strike_rate


def main():
    path1 = "./assets/matches.csv"
    path2 = "./assets/deliveries.csv"
    year_matches.no_of_matches_per_year_main(path1)
    matches_won.matches_won_by_teams_main(path1)
    extra_runs.extra_runs_data_main(path1, path2, 2016)
    economic_bowlers.economic_bowlers_main(path1, path2, 2015)
    top_strike_rate.top_strike_rate_main(path1, path2, 2016, 2016)


if __name__ == '__main__':
    main()
