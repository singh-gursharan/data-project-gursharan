import year_matches
import unittest


class TestIpl(unittest.TestCase):
    test_matches_path = "./assets/matches_test.csv"
    test_deliveries_path = "./assets/deliveries_test.csv"

    def test_year_matches(self):
        result = year_matches.no_of_matches_per_year(TestIpl.test_matches_path)
        output = {
            '2017': 6,
            '2008': 6,
            '2016': 3
        }
        self.assertEqual(result, output)


if __name__ == '__main__':
    unittest.main()
