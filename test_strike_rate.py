import top_strike_rate
import unittest


class Test_strike_rate(unittest.TestCase):
    def test_strike_rate(self):
        result = top_strike_rate.get_top_strike_rates(
            "./assets/matches_test.csv",
            "./assets/deliveries_test.csv", 2017, 2017)
        output = {
            'DA Warner': 100.0,
            'S Dhawan': 66.66666666666666
            }
        self.assertEqual(result, output)


if __name__ == '__main__':
    unittest.main()
